insert into dbssm.description_alarms(error_code,app_name,event_type, alarm_description,active_description) values(000000,'SSM',1,'SSM Health check: SSM module was shutdown, critical','Run SSM module!!!');
insert into dbssm.description_alarms(error_code,app_name,event_type, alarm_description,active_description) values(111111,'Telegram',1,'Telegram runtime error: Telegram system time error, not critical','Check connection telegram bot, run bot!!');
insert into dbssm.description_alarms(error_code,app_name,event_type, alarm_description,active_description) values(222222,'MySql',2,'MySQL Health check: MySQL daemon was shutdown, critical','Check system time');
insert into dbssm.description_alarms(error_code,app_name,event_type, alarm_description,active_description) values(333333,'ApacheTomcat',1,'Tomcat Health check: Tomcat start port error, critical','Check start port');
insert into dbssm.description_alarms(error_code,app_name,event_type, alarm_description,active_description) values(444444,'Telegram',2,'Telegram name error: telegram bot name change,not critical','Refresh telegram bot name');
insert into dbssm.description_alarms(error_code,app_name,event_type, alarm_description,active_description) values(555555,'SipApp',1,'MySQL Health check: MySQL daemon was shutdown, critical','Run MySql daemon!!!');
insert into dbssm.description_alarms(error_code,app_name,event_type, alarm_description,active_description) values(666666,'Statistics',1,'Statistics Health check: Statistics lost connection to database critical','Check connection to database');
insert into dbssm.description_alarms(error_code,app_name,event_type, alarm_description,active_description) values(777777,'SipApp',2,'SipApp Time check:SipApp system time change, not critical','Check system time');
insert into dbssm.description_alarms(error_code,app_name,event_type, alarm_description,active_description) values(888888,'GUI',2,'GUI Font check: GUI error loading font for page, not critical','Check font file');
insert into dbssm.description_alarms(error_code,app_name,event_type, alarm_description,active_description) values(999999,'GUI',1,'GUI Health check: GUI lost connection to sipp module, critical','Run GUI module');

insert into dbgui.phone_numbers(id_phone_number,phone_number) values(1,'+375290000001');
insert into dbgui.phone_numbers(id_phone_number,phone_number) values(2,'+375290000002');
insert into dbgui.phone_numbers(id_phone_number,phone_number) values(3,'+375290000003');

insert into dbgui.abonent(id_abonent,login,name,password,surname,id_phone_number) values(1,'Ega','Alex','ega123','Vasechkin',1);
insert into dbgui.abonent(id_abonent,login,name,password,surname,id_phone_number) values(2,'Gow','Pasha','gow123','Pupkin',2);
insert into dbgui.abonent(id_abonent,login,name,password,surname,id_phone_number) values(3,'Lol','Nastya','lol123','Ivanova',3);

insert into dbgui.white_list(id_white_list,id_phone_number) values(1,1);

insert into dbgui.black_list(id_black_list,id_abonent) values(1,2);
