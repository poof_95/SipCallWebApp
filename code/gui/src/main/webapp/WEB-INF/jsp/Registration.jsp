<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <style>
        <%@include file="css/style.css"%>
        <%@include file="/WEB-INF/jsp/js/jquery-3.3.1.min.js"%>
    </style>
    <script><%@include file="/WEB-INF/jsp/js/jquery-3.3.1.min.js"%></script>
    <title>Registration</title>
</head>

<body>
<form action="${pageContext.request.contextPath}/reg" method="post">



    <div id="login-box">
        <div class="left">
            <center>
                <h1>Registration</h1>
            </center>
            <div class="input-h">
                <h2 class="hidden">Login</h2>
                <input id='login' type="text" name="login" placeholder="Login" required title="This field is enabled for all symbols except Cyrillic" pattern="[a-zA-Z0-9]+$" />
            </div>
            <div class="input-h">
                <h2 class="hidden1">Name</h2>
                <input id='name' type="text" name="name" placeholder="Name" required title="The only Latin letter in any case" pattern="[a-zA-Z]+$">

            </div>
            <div class="input-h">
                <h2 class="hidden2">Surname</h2>
                <input id='surname' type="text" name="surname" placeholder="Surname" required title="The only Latin letter in any case" pattern="[a-zA-Z]+$" />

            </div>

            <div class="input-h">
                <h2 class="hidden3">Password</h2>
                <input id='password' type="password" name="password" placeholder="Password" required title="Latin letter in any register and numbers" pattern="[a-zA-Z0-9]{4,}" />
            </div>
            <div class="input-h">
                <h2 class="hidden5">Phone number</h2>
                <input id='phone' type="text" name="phoneNumber" placeholder="Phone number" required pattern="^\+[0-9]{3,3}[0-9]{9,9}$" />
            </div>
            <div class="submit-registration">
                <input type="submit" name="submit" value="registration"/>
            </div>
        </div>


    </div>


    </div>
</form>
</body>

<script>
    var size = 470;

    $('#login').on('keyup', function() {
        val = $(this).val();
        if (val.length >= 1) {
            $('.hidden').css("opacity", 1);
        } else {
            $('.hidden').css("opacity", 0);
        }
    });
    $('#name').on('keyup', function() {
        val = $(this).val();
        if (val.length >= 1) {
            $('.hidden1').css("opacity", 1);
        } else {
            $('.hidden1').css("opacity", 0);
        }
    });
    $('#surname').on('keyup', function() {
        val = $(this).val();
        if (val.length >= 1) {
            $('.hidden2').css("opacity", 1);
        } else {
            $('.hidden2').css("opacity", 0);
        }
    });

    $('#password').on('keyup', function() {
        val = $(this).val();
        if (val.length >= 1) {
            $('.hidden3').css("opacity", 1);
        } else {
            $('.hidden3').css("opacity", 0);
        }
    });
    $('#password2').on('keyup', function() {
        val = $(this).val();
        if (val.length >= 1) {
            $('.hidden4').css("opacity", 1);
        } else {
            $('.hidden4').css("opacity", 0);
        }
    });
    $('#phone').on('keyup', function() {
        val = $(this).val();
        if (val.length >= 1) {
            $('.hidden5').css("opacity", 1);
        } else {
            $('.hidden5').css("opacity", 0);
        }
    });
</script>

</html>
