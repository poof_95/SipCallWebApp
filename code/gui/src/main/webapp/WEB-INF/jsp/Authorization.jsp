<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <style>
        <%@include file="css/style.css"%>
        <%@include file="js/jquery-3.3.1.min.js"%>
    </style>
    <script><%@include file="js/jquery-3.3.1.min.js"%></script>
    <title>Authorization</title>
</head>

<body>

<form class="" action="/out" method="post">



    <div id="login-box-authorization">
        <div class="left">

            <h1>Authorization</h1>

            <div class="input-h">
                <h2 class="hidden">Login</h2>
                <input id='login' type="text" name="login" placeholder="Login" required title="This field is enabled for all symbols except Cyrillic" pattern="[a-zA-Z0-9]+$" />
            </div>
            <div class="input-h">
                <h2 class="hidden3">Password</h2>
                <input id='password' type="password" name="password" placeholder="Password" required title="Latin letter in any register and numbers" pattern="[a-zA-Z0-9]{4,}" />
            </div>
            <div class="submit-authorization">
                <input type="submit" name="submit" value="Authorization" />
            </div>
        </div>


    </div>


    </div>
</form>
</body>
<script>
    var size = 470;

    $('#login').on('keyup', function() {
        val = $(this).val();
        if (val.length >= 1) {
            $('.hidden').css("opacity", 1);
        } else {
            $('.hidden').css("opacity", 0);
        }
    });
    $('#password').on('keyup', function() {
        val = $(this).val();
        if (val.length >= 1) {
            $('.hidden3').css("opacity", 1);
        } else {
            $('.hidden3').css("opacity", 0);
        }
    });
</script>

</html>
