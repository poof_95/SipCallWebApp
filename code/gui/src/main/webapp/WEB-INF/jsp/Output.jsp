<%@ page import="com.sipapp.models.Abonent" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <style>
        <%@include file="css/bootstrap.min.css"%>
        <%@include file="css/main.css"%>
    </style>
    <title>User List</title>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
            <div id="login-box-output">
                <div class="left">
                    <h1>User List</h1>
                    <table class="table table-responsive" style="width: 100%;">
                        <tr>
                            <th>login</th>
                            <th>name</th>
                            <th>surname</th>
                            <th>phone</th>
                            <th><a class="btn btn-secondary" href="${pageContext.request.contextPath}/index">Main</a></th>
                        </tr>
                        <c:forEach items="${list}" var="abonent">
                            <tr>
                                <td><label type="text" id="login">${abonent.login}</label> </td>
                                <td><label type="text" id="name">${abonent.name}</label></td>
                                <td><label type="text" id="surname">${abonent.surname}</label></td>
                                <td><label type="text" id="phoneNumber">${abonent.phoneNumber.phoneNumber}</label></td>
                                <td>
                                    <a href="${pageContext.request.contextPath}/edit/${abonent.idAbonent}"><button type="submit" class="btn btn-info" onclick="<%request.getSession().setAttribute("abonent",(Abonent)pageContext.getAttribute("abonent"));%>">Edit</button></a>
                                </td>
                                <td>
                                    <a href="${pageContext.request.contextPath}/delete/${abonent.idAbonent}"><button type="submit" class="btn btn-danger">Delete</button></a>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </div>
    </div>
</div>
</div>

</body>
</html>
