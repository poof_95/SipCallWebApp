<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en" dir="ltr">
<head>

    <meta charset="utf-8">
    <style>
        <%@include file="css/style_new.css"%>
        <%@include file="css/bootstrap.min.css"%>
    </style>
    <title></title>
</head>

<body>
    <section id="login-box-failure">
        <h2 align="center">Error</h2>
        <div class="container">
            <br />
            <br />
            <div class="row">
                <div class="col-md-6">
                    <a style="float: right;" href="/index"><input type="button" value="Main"id="button-main" class="btn btn-dark" /></a>
                </div>
                <div class="col-md-6">
                    <a href="/reg"><input type="button" value="Registration" id="button-auto" class="btn btn-dark" /></a>
                </div>
            </div>
        </div>



    </section>

</body>

</html>
