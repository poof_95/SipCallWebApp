<%@ page import="com.sipapp.models.Abonent" %>
<%@ page import="java.util.List" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <style>
        <%@include file="css/style.css"%>
    </style>
    <script><%@include file="js/jquery-3.3.1.min.js"%></script>
    <title>Edit</title>
</head>
<body>
<form class="" action="${pageContext.request.contextPath}/edit/update" method="post">
    <div id="login-box">
        <div class="left">
            <h1>Edit</h1>
            <div class="input-h">
                <h2 class="hidden">Login</h2>
                <input id='login' type="text" name="login" placeholder="Login" value=${abonent.login} required pattern="[a-zA-Z0-9]+$" readonly />
            </div>
            <div class="input-h">
                <h2 class="hidden1">Name</h2>
                <input id='name' type="text" name="name" placeholder="Name" value=${abonent.name} required pattern="[a-zA-Z]+$" />

            </div>
            <div class="input-h">
                <h2 class="hidden2">Surname</h2>
                <input id='surname' type="text" name="surname" placeholder="Surname" value=${abonent.surname} required pattern="[a-zA-Z]+$" />

            </div>
            <div class="input-h">
                <h2 class="hidden3">Phone number</h2>
                <input id='phone' type="text" name="phoneNumber" placeholder="Phone number" value=${abonent.phoneNumber.phoneNumber} required  pattern="^\+[0-9]{3,3}[0-9]{9,9}$" />

            </div>
            <div class="input-h">
                <h2 class="hidden4">Password</h2>
                <input id='password' type="password" name="password" placeholder="Current password" value=${abonent.password} required pattern="[a-zA-Z0-9]{4,}" readonly />
            </div>
            <div class="submit-edit">
                <input type="submit" name="submit" value="change" />
            </div>
        </div>
    </div>


    </div>
</form>
</body>

<script>
    var size = 470;

    $('#login').on('keyup', function() {
        val = $(this).val();
        if (val.length >= 1) {
            $('.hidden').css("opacity", 1);

        } else {
            $('.hidden').css("opacity", 0);

        }
    });
    $('#name').on('keyup', function() {
        val = $(this).val();
        if (val.length >= 1) {
            $('.hidden1').css("opacity", 1);
        } else {
            $('.hidden1').css("opacity", 0);
        }
    });
    $('#surname').on('keyup', function() {
        val = $(this).val();
        if (val.length >= 1) {
            $('.hidden2').css("opacity", 1);
        } else {
            $('.hidden2').css("opacity", 0);
        }
    });
    $('#phone').on('keyup', function() {
        val = $(this).val();
        if (val.length >= 1) {
            $('.hidden3').css("opacity", 1);
        } else {
            $('.hidden3').css("opacity", 0);
        }
    });
    $('#password').on('keyup', function() {
        val = $(this).val();
        if (val.length >= 1) {
            $('.hidden4').css("opacity", 1);
        } else {
            $('.hidden4').css("opacity", 0);
        }
    });
    $('#password2').on('keyup', function() {
        val = $(this).val();
        if (val.length >= 1) {
            $('.hidden5').css("opacity", 1);
        } else {
            $('.hidden5').css("opacity", 0);
        }
    });
</script>

</html>