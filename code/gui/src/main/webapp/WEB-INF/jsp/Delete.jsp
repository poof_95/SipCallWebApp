<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <style>
        <%@include file="css/style.css"%>
        <%@include file="js/jquery-3.3.1.min.js"%>
    </style>
    <script><%@include file="js/jquery-3.3.1.min.js"%></script>
    <title>Delete</title>

<body>
</head>
<script>
    function clicked(e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    }
</script>
<form class="" action="${pageContext.request.contextPath}/delete/${abonent.idAbonent}" method="post">



    <div id="login-box-delete">
        <div class="left">
                <h1>Delete</h1>
            <div class="input-h">
                <h2 class="hidden">Login</h2>
                <input id='login' type="text" name="login" value=${abonent.login} placeholder="Login" required readonly />
            </div>
            <div class="input-h">
                <h2 class="hidden1">Name</h2>
                <input id='name' type="text" name="name" placeholder="Name" value=${abonent.name} required readonly />

            </div>
            <div class="input-h">
                <h2 class="hidden2">Surname</h2>
                <input id='surname' type="text" name="surname" placeholder="Surname" value=${abonent.surname} required readonly />

            </div>
            <div class="input-h">
                <h2 class="hidden3">Phone number</h2>
                <input id='phone' type="text" name="phonenumber" placeholder="Phone number" value=${abonent.phoneNumber.phoneNumber} required readonly />

            </div>

            <div class="submit-delete" >
                <input type="submit" name="submit" value="delete" onclick="clicked(event)"/>
            </div>


            <div class="submit-back">
                <a href="/reg">
                    <input class="button-back" type="button" name="submit-back" value="back" />
                </a>
            </div>
        </div>


    </div>


    </div>
</form>
</body>
<script src="js/jquery-3.3.1.min.js"></script>
<script>
    var size = 470;

    $('#login').on('keyup', function() {
        val = $(this).val();
        if (val.length >= 1) {
            $('.hidden').css("opacity", 1);
        } else {
            $('.hidden').css("opacity", 0);
        }
    });
    $('#name').on('keyup', function() {
        val = $(this).val();
        if (val.length >= 1) {
            $('.hidden1').css("opacity", 1);
        } else {
            $('.hidden1').css("opacity", 0);
        }
    });
    $('#surname').on('keyup', function() {
        val = $(this).val();
        if (val.length >= 1) {
            $('.hidden2').css("opacity", 1);
        } else {
            $('.hidden2').css("opacity", 0);
        }
    });
    $('#phone').on('keyup', function() {
        val = $(this).val();
        if (val.length >= 1) {
            $('.hidden3').css("opacity", 1);
        } else {
            $('.hidden3').css("opacity", 0);
        }
    });
</script>

</html>