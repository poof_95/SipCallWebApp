package com.sipapp.services;

import com.sipapp.models.*;
import com.sipapp.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GuiServiceImpl implements GuiService {

    private final AbonentRepository abonentRepository;
    private final BlackListRepository blackListRepository;
    private final CallsRepository callsRepository;
    private final PhoneNumberRepository phoneNumberRepository;
    private final WhiteListRepository whiteListRepository;

    @Autowired
    public GuiServiceImpl(AbonentRepository abonentRepository, BlackListRepository blackListRepository, CallsRepository callsRepository, PhoneNumberRepository phoneNumberRepository, WhiteListRepository whiteListRepository) {
        this.abonentRepository = abonentRepository;
        this.blackListRepository = blackListRepository;
        this.callsRepository = callsRepository;
        this.phoneNumberRepository = phoneNumberRepository;
        this.whiteListRepository = whiteListRepository;
    }

    @Override
    public String editAbonent(Abonent abonent) {
        Abonent old_abonent = finaAbonentbyLogin(abonent.getLogin());
        if(old_abonent == null){
            return "Failure";
        }
        else{
            old_abonent.setName(abonent.getName());
            old_abonent.setSurname(abonent.getSurname());
            abonentRepository.save(old_abonent);
            return "Success";
        }
    }

    @Override
    public String addAbonent(Abonent abonent) {
        if(finaAbonentbyLogin(abonent.getLogin()) == null){
            abonentRepository.save(abonent);
            return "Success";
        }
        else {
            return "Failure";
        }
    }

    @Override
    public List<Abonent> getAbonents() {
        return (List<Abonent>) abonentRepository.findAll();
    }

    @Override
    public String deleteAbonent(Long id) {

        Optional<Abonent> old_abonent = abonentRepository.findById(id);
        if(!old_abonent.isPresent()){
            return "Failure";
        }
        else {
                abonentRepository.delete(old_abonent.get());
                return "Success";
        }

    }

    @Override
    public void saveAbonentToWhiteList(PhoneNumber phoneNumber) {
        whiteListRepository.save(new WhiteList(phoneNumber));
    }

    @Override
    public void deleteFromWhiteList(Long id) {
        whiteListRepository.deleteById(id);
    }

    @Override
    public void saveAbonentToBlackList(Abonent abonent) {
        blackListRepository.save(new BlackList(abonent));
    }

    @Override
    public void deleteFromBlackList(Long id) {
        blackListRepository.deleteById(id);
    }

    @Override
    public void addCall(Calls calls) {
        callsRepository.save(calls);
    }

    @Override
    public void deleteCall(Long id) {
        callsRepository.deleteById(id);
    }

    @Override
    public List<Calls> getCalls() {
        return (List<Calls>) callsRepository.findAll();
    }

    @Override
    public List<Calls> getCalls(Abonent abonent) {
        List<Calls> calls = (List<Calls>) callsRepository.findAll();
        for (Calls call : calls) {
            if (!call.getAbonent().getIdAbonent().equals(abonent.getIdAbonent()))
                calls.remove(call);
        }
        return calls;
    }

    @Override
    public List<Abonent> findAbonentsByName(String name) {
        List<Abonent> abonents = (List<Abonent>) abonentRepository.findAll();
        for (Abonent abonent : abonents) {
            if (!abonent.getName().equals(name)) {
                abonents.remove(abonent);
            }
        }
        return abonents;
    }

    @Override
    public List<BlackList> getBlackList() {
        List<BlackList> blackLists = (List<BlackList>) blackListRepository.findAll();
        return blackLists;
    }

    @Override
    public Abonent findAbonentById(Long Id) {
        return abonentRepository.findById(Id).get();
    }

    @Override
    public Boolean authorization(Abonent abonent) {
        Abonent old_abonent = finaAbonentbyLogin(abonent.getLogin());
        if(old_abonent == null){
            return false;
        }
        else if(old_abonent.getPassword().equals(abonent.getPassword())){
            return true;
        }else
            return false;
    }

    private Abonent finaAbonentbyLogin(String login){
        List<Abonent> abonents = (List<Abonent>) abonentRepository.findAll();
        for(Abonent abonent : abonents){
            if(abonent.getLogin().equals(login))
                return abonent;
        }
        return null;
    }
}
