package com.sipapp.services;

import com.sipapp.models.*;

import java.util.List;

public interface GuiService {
    String editAbonent(Abonent abonent);

    String addAbonent(Abonent abonent);

    List<Abonent> getAbonents();

    String deleteAbonent(Long id);

    void saveAbonentToWhiteList(PhoneNumber phoneNumber);

    void deleteFromWhiteList(Long id);

    void saveAbonentToBlackList(Abonent abonent);

    void deleteFromBlackList(Long id);

    void addCall(Calls calls);

    void deleteCall(Long id);

    List<Calls> getCalls();

    List<Calls> getCalls(Abonent abonent);

    List<Abonent> findAbonentsByName(String name);

    List<BlackList> getBlackList();

    Abonent findAbonentById(Long Id);;

    Boolean authorization(Abonent abonent);
}
