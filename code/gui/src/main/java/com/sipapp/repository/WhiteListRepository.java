package com.sipapp.repository;

import com.sipapp.models.WhiteList;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WhiteListRepository extends CrudRepository<WhiteList, Long> {
}
