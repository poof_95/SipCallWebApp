package com.sipapp.repository;


import com.sipapp.models.Abonent;
import com.sun.xml.internal.bind.v2.model.core.ID;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AbonentRepository extends CrudRepository<Abonent,Long> {
}
