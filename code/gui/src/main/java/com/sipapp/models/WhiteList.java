package com.sipapp.models;

import javax.persistence.*;


@Entity
@Table(name = "white_list")
public class WhiteList {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idWhiteList", nullable = false)
    private Long idWhiteList;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_phone_number", nullable = false)
    private PhoneNumber phoneNumber;

    public WhiteList() {
    }

    public WhiteList(PhoneNumber phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Long getIdWhiteList() {
        return idWhiteList;
    }

    public void setIdWhiteList(Long idWhiteList) {
        this.idWhiteList = idWhiteList;
    }

    public PhoneNumber getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(PhoneNumber phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
