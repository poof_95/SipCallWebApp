package com.sipapp.models;


import javax.persistence.*;

@Entity
@Table(name = "abonent")
public class Abonent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, name = "idAbonent")
    private Long idAbonent;
    @Column(unique = true, length = 20, nullable = false)
    private String login;
    @Column(length = 20, nullable = false)
    private String password;
    @Column(length = 20, nullable = false)
    private String name;
    @Column(length = 20, nullable = false)
    private String surname;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_phone_number", nullable = false)
    private PhoneNumber phoneNumber;

    public Abonent() {
    }

    public Abonent(String login, String password, String name, String surname, String phoneNumber) {
        this.login = login;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.phoneNumber = new PhoneNumber(phoneNumber);
    }

    public Long getIdAbonent() {
        return idAbonent;
    }

    public void setIdAbonent(Long idAbonent) {
        this.idAbonent = idAbonent;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public PhoneNumber getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = new PhoneNumber(phoneNumber);
    }
}