package com.sipapp.models;

import javax.persistence.*;

@Entity
@Table(name = "black_list")
public class BlackList {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idBlackList", nullable = false)
    private Long idBlackList;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_abonent", nullable = true)
    private Abonent abonent;

    public BlackList() {
    }

    public BlackList(Abonent abonent) {
        this.abonent = abonent;
    }

    public Long getIdBlackList() {
        return idBlackList;
    }

    public void setIdBlackList(Long idBlackList) {
        this.idBlackList = idBlackList;
    }

    public Abonent getAbonent() {
        return abonent;
    }

    public void setAbonent(Abonent abonent) {
        this.abonent = abonent;
    }
}