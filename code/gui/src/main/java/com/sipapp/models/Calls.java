package com.sipapp.models;

import javax.persistence.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Date;

@Entity
@Table(name = "calls")
public class Calls {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, name = "idCall")
    private Long idCall;
    @Column(nullable = false)
    @Temporal(value = TemporalType.DATE)
    private Date dateCall;
    @Column(nullable = false)
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date startCall;
    @Column(nullable = false)
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date endCall;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_abonent")
    private Abonent abonent;

    public Calls() {
    }

    public Calls(Date dateCall, Date startCall, Date endCall, Abonent abonent) {
        this.dateCall = dateCall;
        this.startCall = startCall;
        this.endCall = endCall;
        this.abonent = abonent;
    }

    public Long getIdCall() {
        return idCall;
    }

    public void setIdCall(Long idCall) {
        this.idCall = idCall;
    }

    public Date getDateCall() {
        return dateCall;
    }

    public void setDateCall(Date dateCall) {
        this.dateCall = dateCall;
    }

    public Date getStartCall() {
        return startCall;
    }

    public void setStartCall(Date startCall) {
        this.startCall = startCall;
    }

    public Date getEndCall() {
        return endCall;
    }

    public void setEndCall(Date endCall) {
        this.endCall = endCall;
    }

    public Abonent getAbonent() {
        return abonent;
    }

    public void setAbonent(Abonent abonent) {
        this.abonent = abonent;
    }
}
