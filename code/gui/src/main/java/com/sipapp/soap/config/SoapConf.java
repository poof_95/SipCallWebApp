package com.sipapp.soap.config;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;

import org.springframework.xml.xsd.XsdSchema;
@Configuration
@EnableWs
public class SoapConf {
    @Bean
    public ServletRegistrationBean messageDispatcherServlet(ApplicationContext applicationContext){
        MessageDispatcherServlet servlet = new MessageDispatcherServlet();
        servlet.setApplicationContext(applicationContext);
        servlet.setTransformSchemaLocations(true);
        return new ServletRegistrationBean(servlet,"/ws/*");
    }

    @Bean(name = "abonents")
    public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema abonentsSchema){
        DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName("AbonentPort");
        wsdl11Definition.setLocationUri("/ws");
        wsdl11Definition.setTargetNamespace("localhost:8081/com/sipapp/soap/classes");
        wsdl11Definition.setSchema(abonentsSchema);
        return wsdl11Definition;
    }

    @Bean
    public XsdSchema abonentsSchema(){
        return new SimpleXsdSchema(new ClassPathResource("abonents.xsd"));
    }
}
