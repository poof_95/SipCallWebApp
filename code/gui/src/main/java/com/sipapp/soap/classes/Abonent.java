package com.sipapp.soap.classes;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Abonent", propOrder = {
        "login",
        "password",
        "name",
        "surname",
        "phoneNumber",
         "abonent"
})
public class Abonent {

    @XmlElement
    private com.sipapp.models.Abonent abonent;
    @XmlElement(required = true)
    private String login;
    @XmlElement(required = true)
    private String password;
    @XmlElement(required = true)
    private String name;
    @XmlElement(required = true)
    private String surname;
    @XmlElement(required = true)
    private PhoneNumber phoneNumber;

    public Abonent() {
    }

    public Abonent(String login, String password, String name, String surname, PhoneNumber phoneNumber) {
        this.login = login;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.phoneNumber = phoneNumber;
    }

    public Abonent(com.sipapp.models.Abonent abonent){
        this.abonent = abonent;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public PhoneNumber getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(PhoneNumber phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public com.sipapp.models.Abonent getAbonent() {
        return abonent;
    }

    public void setAbonent(com.sipapp.models.Abonent abonent) {
        this.abonent = abonent;
    }
}