package com.sipapp.soap.classes;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "Abonent"
})
@XmlRootElement(name = "deleteAbonentRequest")
public class DeleteAbonentRequest {
    @XmlElement(name = "deleteAbonent", required = true)
    private Abonent Abonent;

    public Abonent getIdAbonent() {
        return Abonent;
    }

    public void setIdAbonent(Abonent abonent) {
        this.Abonent = abonent;
    }
}
