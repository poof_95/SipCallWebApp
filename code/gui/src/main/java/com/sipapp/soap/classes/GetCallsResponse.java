package com.sipapp.soap.classes;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "callList"
})
@XmlRootElement(name = "getCallsResponse")
public class GetCallsResponse {

    private List<Calls> callList;

    public List<Calls> getCallList() {
        return callList;
    }

    public void setCallList(List<Calls> callList) {
        this.callList = callList;
    }
}
