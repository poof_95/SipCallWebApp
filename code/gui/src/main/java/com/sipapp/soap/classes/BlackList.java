package com.sipapp.soap.classes;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BlackList", propOrder = {
        "idBlackList",
        "abonent"
})
public class BlackList {
    @XmlElement(required = true)
    private Long idBlackList;
    @XmlElement(required = true)
    private Abonent abonent;

    public BlackList() {
    }

    public BlackList(Abonent abonent) {
        this.abonent = abonent;
    }

    public Long getIdBlackList() {
        return idBlackList;
    }

    public void setIdBlackList(Long idBlackList) {
        this.idBlackList = idBlackList;
    }

    public Abonent getAbonent() {
        return abonent;
    }

    public void setAbonent(Abonent abonent) {
        this.abonent = abonent;
    }
}