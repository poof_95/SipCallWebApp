package com.sipapp.soap.services;

import com.sipapp.services.GuiService;
import com.sipapp.soap.classes.Abonent;
import com.sipapp.soap.classes.BlackList;
import com.sipapp.soap.classes.Calls;
import com.sipapp.soap.endpoints.SoapEndpoint;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Service
public class SoapService {
    private final GuiService service;
    private static final Logger log = Logger.getLogger(SoapEndpoint.class.getName());

    public SoapService(GuiService service) {
        this.service = service;
    }

    public void createAbonent(Abonent soapAbonent){
        com.sipapp.models.Abonent abonent = new com.sipapp.models.Abonent();
        abonent.setLogin(soapAbonent.getLogin());
        abonent.setName(soapAbonent.getName());
        abonent.setPassword(soapAbonent.getPassword());
        abonent.setSurname(soapAbonent.getSurname());
        abonent.setPhoneNumber(soapAbonent.getPhoneNumber().getPhoneNumber());
        service.addAbonent(abonent);
    }

    public List<Abonent> getAbonents(){
        List<com.sipapp.models.Abonent> bufAbonents = service.getAbonents();
        List<Abonent> abonents = new ArrayList<>();
        for (com.sipapp.models.Abonent abonent : bufAbonents) {
            abonents.add(new Abonent(
                    abonent.getLogin(),
                    abonent.getPassword(),
                    abonent.getName(),
                    abonent.getSurname(),
                    new com.sipapp.soap.classes.PhoneNumber(abonent.getPhoneNumber().getPhoneNumber())));
        }
        return abonents;
    }

    public void deleteAbonent(Abonent soapAbonent) {
        com.sipapp.models.Abonent abonent = new com.sipapp.models.Abonent();
        abonent.setLogin(soapAbonent.getLogin());
        abonent.setName(soapAbonent.getName());
        abonent.setPassword(soapAbonent.getPassword());
        abonent.setSurname(soapAbonent.getSurname());
        abonent.setPhoneNumber(soapAbonent.getPhoneNumber().getPhoneNumber());
        service.deleteAbonent(abonent.getIdAbonent());
    }

    public void saveAbonentToBlackList(Abonent blackListAbonent) {
        log.info("SERVICE");
        List<com.sipapp.models.Abonent> bufAbonents = service.getAbonents();
        com.sipapp.models.Abonent abonent = new com.sipapp.models.Abonent();

        abonent.setPhoneNumber(blackListAbonent.getAbonent().getPhoneNumber().getPhoneNumber());
        abonent.setLogin(blackListAbonent.getLogin());
        abonent.setPassword(blackListAbonent.getPassword());
        abonent.setName(blackListAbonent.getName());
        abonent.setSurname(blackListAbonent.getSurname());


        service.saveAbonentToBlackList(abonent);
    }

    public void deleteFromBlackList(Long id) {
        service.deleteFromBlackList(id);
    }

    public void deleteCall(Long id) {
        service.deleteCall(id);
    }


    public void addCall(Calls calls) {
        com.sipapp.models.Calls call= new com.sipapp.models.Calls();
        com.sipapp.models.Abonent abonent = new com.sipapp.models.Abonent ();

        abonent.setLogin(calls.getAbonent().getLogin());
        abonent.setPassword(calls.getAbonent().getPassword());
        abonent.setName(calls.getAbonent().getName());
        abonent.setSurname(calls.getAbonent().getSurname());
        abonent.setPhoneNumber(calls.getAbonent().getPhoneNumber().getPhoneNumber());

        call.setAbonent(abonent);
        call.setStartCall(calls.getStartCall());
        call.setEndCall(calls.getEndCall());
        call.setDateCall(calls.getDateCall());
        call.setIdCall(calls.getIdCall());

        service.addCall(call);
    }

    public List<Calls> getCalls() {
        List<com.sipapp.models.Calls> bufCalls = service.getCalls();
        List<Calls> calls = new ArrayList<>();
        for (com.sipapp.models.Calls call : bufCalls) {
            calls.add(new Calls(call.getIdCall(),
                    call.getDateCall(),
                    call.getStartCall(),
                    call.getEndCall(),
                    new com.sipapp.soap.classes.Abonent(call.getAbonent().getLogin(),
                            call.getAbonent().getPassword(),
                            call.getAbonent().getName(),
                            call.getAbonent().getSurname(),
                            new com.sipapp.soap.classes.PhoneNumber(call.getAbonent().getPhoneNumber().getPhoneNumber()))));
        }
        return calls;
    }

    public List<Abonent> findAbonentsByName(String name) {
        List<com.sipapp.models.Abonent> bufAbonents = service.findAbonentsByName(name);
        List<Abonent> abonents = new ArrayList<>();
        for (com.sipapp.models.Abonent abonent : bufAbonents) {
            abonents.add(new Abonent(
                    abonent.getLogin(),
                    abonent.getPassword(),
                    abonent.getName(),
                    abonent.getSurname(),
                    new com.sipapp.soap.classes.PhoneNumber(abonent.getPhoneNumber().getPhoneNumber())));
        }
        return abonents;
    }

    public List<BlackList> getBlackList() {
        List<com.sipapp.models.BlackList> blackList = service.getBlackList();
        List<BlackList> black = new ArrayList<>();
        for(com.sipapp.models.BlackList list : blackList){
            black.add(new BlackList(new Abonent(list.getAbonent())));
        }

        return black;

    }
}