package com.sipapp.soap.classes;

import javax.xml.bind.annotation.XmlRegistry;

@XmlRegistry
public class ObjectFactory {

    public ObjectFactory() {
    }

    public Abonent createAbonent(){
        return new Abonent();
    }

    public BlackList createBlackList(){
        return new BlackList();
    }

    public Calls createCalls(){
        return new Calls();
    }

    public PhoneNumber createPhoneNumber(){
        return new PhoneNumber();
    }

    public WhiteList createWhiteList(){
        return new WhiteList();
    }

}
