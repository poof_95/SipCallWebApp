package com.sipapp.soap.classes;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "Call"
})
@XmlRootElement(name = "deleteCallRequest")
public class DeleteCallRequest {
    @XmlElement(name = "id", required = true)
    private Long Call;

    public Long getIdCall() {
        return Call;
    }

    public void setIdCall(Long Call) {
        this.Call = Call;
    }
}
