package com.sipapp.soap.classes;

import javax.xml.bind.annotation.*;

/**
 * @author Ihar Kharuzhy
 * 7/2/2018.
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "status"
})
@XmlRootElement(name = "addCallResponse")
public class AddCallResponse {
    @XmlElement(required = true)
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
