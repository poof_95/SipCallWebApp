package com.sipapp.soap.classes;

import javax.xml.bind.annotation.*;

/**
 * @author Ihar Kharuzhy
 * 7/2/2018.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "name"
})
@XmlRootElement(name = "findAbonentsByNameRequest")
public class FindAbonentsByNameRequest {
    @XmlElement(name = "name", required = true)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String abonent) {
        name = abonent;
    }
}
