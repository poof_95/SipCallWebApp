package com.sipapp.soap.classes;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

/**
 * @author Ihar Kharuzhy
 * 7/2/2018.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "abonentList"
})
@XmlRootElement(name = "findAbonentsByNameResponse")
public class FindAbonentsByNameResponse {

    private List<Abonent> abonentList;

    public List<Abonent> getAbonentList() {
        return abonentList;
    }

    public void setAbonentList(List<Abonent> abonentList) {
        this.abonentList = abonentList;
    }
}
