package com.sipapp.soap.classes;

import javax.xml.bind.annotation.*;

/**
 * @author Ihar Kharuzhy
 * 7/2/2018.
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "call"
})
@XmlRootElement(name = "addCallRequest")
public class AddCallRequest {
    @XmlElement(name = "Call", required = true)
    private Calls call;

    public Calls getCall() {
        return call;
    }

    public void setCall(Calls call) {
        this.call = call;
    }
}
