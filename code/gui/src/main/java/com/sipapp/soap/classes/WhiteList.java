package com.sipapp.soap.classes;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WhiteList", propOrder = {
        "idWhiteList",
        "phoneNumber"
})
public class WhiteList {

    @XmlElement(required = true)
    private Long idWhiteList;
    @XmlElement(required = true)
    private PhoneNumber phoneNumber;

    public WhiteList() {
    }

    public WhiteList(PhoneNumber phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Long getIdWhiteList() {
        return idWhiteList;
    }

    public void setIdWhiteList(Long idWhiteList) {
        this.idWhiteList = idWhiteList;
    }

    public PhoneNumber getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(PhoneNumber phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
