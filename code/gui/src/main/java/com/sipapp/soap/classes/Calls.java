package com.sipapp.soap.classes;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Date;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Calls", propOrder = {
        "idCall",
        "dateCall",
        "startCall",
        "endCall",
        "abonent"
})
public class Calls {
    @XmlElement(required = true)
    private Long idCall;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    private Date dateCall;
    @XmlSchemaType(name = "date")
    @XmlElement(required = true)
    private Date startCall;
    @XmlSchemaType(name = "date")
    @XmlElement(required = true)
    private Date endCall;
    @XmlElement(required = true)
    private Abonent abonent;

    public Calls() {
    }

    public Calls(Long idCall, Date dateCall, Date startCall, Date endCall, Abonent abonent) {
        this.idCall = idCall;
        this.dateCall = dateCall;
        this.startCall = startCall;
        this.endCall = endCall;
        this.abonent = abonent;
    }

    public Long getIdCall() {
        return idCall;
    }

    public void setIdCall(Long idCall) {
        this.idCall = idCall;
    }

    public Date getDateCall() {
        return dateCall;
    }

    public void setDateCall(Date dateCall) {
        this.dateCall = dateCall;
    }

    public Date getStartCall() {
        return startCall;
    }

    public void setStartCall(Date startCall) {
        this.startCall = startCall;
    }

    public Date getEndCall() {
        return endCall;
    }

    public void setEndCall(Date endCall) {
        this.endCall = endCall;
    }

    public Abonent getAbonent() {
        return abonent;
    }

    public void setAbonent(Abonent abonent) {
        this.abonent = abonent;
    }
}
