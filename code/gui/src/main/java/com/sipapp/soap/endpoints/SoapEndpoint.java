package com.sipapp.soap.endpoints;

import com.sipapp.soap.classes.*;
import com.sipapp.soap.services.SoapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.ArrayList;
import java.util.logging.Logger;

@Endpoint
public class SoapEndpoint {
    private static final String NAMESPACE_URL = "localhost:8081/com/sipapp/soap/classes";
    private static final Logger log = Logger.getLogger(SoapEndpoint.class.getName());

    private final SoapService service;

    @Autowired
    public SoapEndpoint(SoapService service) {
        this.service = service;
    }

    @PayloadRoot(namespace = NAMESPACE_URL, localPart = "getAbonentsResponse")
    @ResponsePayload
    public GetAbonentsResponse getAbonents() {
        GetAbonentsResponse response = new GetAbonentsResponse();
        try {
            log.info("=====Get abonents=====");
            response.setAbonentList(service.getAbonents());
        } catch (Exception e) {
            log.info("=====Failure get abonents=====");
            response.setAbonentList(new ArrayList<>());
        }
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URL, localPart = "deleteAbonentRequest")
    @ResponsePayload
    public DeleteAbonentResponse deleteAbonent(@RequestPayload DeleteAbonentRequest abonentRequest) {
        DeleteAbonentResponse response = new DeleteAbonentResponse();
        try {
            log.info("=====Abonent delete=====");
            service.deleteAbonent(abonentRequest.getIdAbonent());
            response.setStatus("Success delete abonent!!!");
        } catch (Exception e) {
            log.info(e.getMessage());
            response.setStatus("Failure delete abonent!!!");
        }
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URL, localPart = "saveAbonentToBlackListRequest")
    @ResponsePayload
    public SaveAbonentToBlackListResponse saveAbonentToBlackList(@RequestPayload SaveAbonentToBlackListRequest abonent) {
        SaveAbonentToBlackListResponse response = new SaveAbonentToBlackListResponse();
        try {
            log.info("=====Phone number save to black list=====");
            service.saveAbonentToBlackList(abonent.getAbonent());
            response.setStatus("Success save to black list!!!");
        } catch (Exception e) {
            log.info(e.toString());
            response.setStatus("Failure save to black list!!!");
        }
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URL,localPart = "createAbonentRequest")
    @ResponsePayload
    public CreateAbonentResponse createAbonent(@RequestPayload CreateAbonentRequest request){
        CreateAbonentResponse response = new CreateAbonentResponse();
        try {
            log.info("=====Abonent create=====");
            log.info(request.getAbonent().getName());
            service.createAbonent(request.getAbonent());
            response.setStatus("Success create abonent!!!");
        }catch (Exception e){
            log.info(e.getMessage());
            response.setStatus("Failure create abonent!!!");
        }
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URL, localPart = "deleteFromBlackListRequest")
    @ResponsePayload
    public DeleteFromBlackListResponse deleteFromBlackList(@RequestPayload DeleteFromBlackListRequest id) {
        DeleteFromBlackListResponse response = new DeleteFromBlackListResponse();
        try {
            log.info("=====Phone number delete from white list=====");
            service.deleteFromBlackList(id.getIdAbonent());
            response.setStatus("Success delete phone number!!!");
        } catch (Exception e) {
            log.info(e.getMessage());
            response.setStatus("Failure delete phone number!!!");
        }
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URL, localPart = "deleteCallRequest")
    @ResponsePayload
    public DeleteCallResponse deleteCall(@RequestPayload DeleteCallRequest id) {
        DeleteCallResponse response = new DeleteCallResponse();
        try {
            log.info("=====Delete call=====");
            service.deleteCall(id.getIdCall());
            response.setStatus("Success delete call!!!");
        } catch (Exception e) {
            log.info(e.getMessage());
            response.setStatus("Failure delete call!!!");
        }
        return response;
    }

   @PayloadRoot(namespace = NAMESPACE_URL, localPart = "addCallRequest")
   @ResponsePayload
    public AddCallResponse addCall(@RequestPayload AddCallRequest calls) {
        AddCallResponse response = new AddCallResponse();
        try {
            log.info("=====Add call=====");
            service.addCall(calls.getCall());
            response.setStatus("Success add call!!!");
        } catch(Exception e){
            log.info(e.getMessage());
            response.setStatus("Failure add call");
        }
        return response;
    }

        @PayloadRoot(namespace = NAMESPACE_URL, localPart = "getCallsResponse")
        @ResponsePayload
        public GetCallsResponse getCalls() {
            GetCallsResponse response = new GetCallsResponse();
            try {
                log.info("=====Get calls=====");
                response.setCallList(service.getCalls());
            } catch (Exception e) {
                log.info(e.toString());
                log.info("=====Failure get calls=====");
                response.setCallList(new ArrayList<>());
            }
            return response;
        }

    @PayloadRoot(namespace = NAMESPACE_URL, localPart = "findAbonentsByNameRequest")
    @ResponsePayload
    public FindAbonentsByNameResponse getByName(@RequestPayload FindAbonentsByNameRequest name) {
        FindAbonentsByNameResponse response = new FindAbonentsByNameResponse();
        try {
            log.info("=====Get abonents=====");
            response.setAbonentList(service.findAbonentsByName(name.getName()));
        } catch (Exception e) {
            log.info(e.toString());
            log.info("=====Failure get abonents=====");
            response.setAbonentList(new ArrayList<>());
        }
        return response;
    }

   @PayloadRoot(namespace = NAMESPACE_URL, localPart = "getBlackListResponse")
    @ResponsePayload
    public GetBlackListResponse getBlackList() {
       GetBlackListResponse response = new GetBlackListResponse();
       try {
           log.info("=====Get black list=====");
           response.setBlackList(service.getBlackList());
       } catch (Exception e) {
           log.info(e.toString());
           log.info("=====Failure get black list=====");
           response.setBlackList(new ArrayList<>());
       }
       return response;
    }
}
