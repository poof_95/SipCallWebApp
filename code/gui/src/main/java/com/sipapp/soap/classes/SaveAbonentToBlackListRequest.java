package com.sipapp.soap.classes;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "abonent"
})
@XmlRootElement(name = "saveAbonentToBlackListRequest")
public class SaveAbonentToBlackListRequest {
    @XmlElement(name = "Abonent", required = true)
    private Abonent abonent;

    public Abonent getAbonent() {
        return abonent;
    }

    public void setAbonent(Abonent abonent) {
        this.abonent = abonent;
    }
}
