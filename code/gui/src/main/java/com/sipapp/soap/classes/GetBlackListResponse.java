package com.sipapp.soap.classes;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

/**
 * @author Ihar Kharuzhy
 * 7/3/2018.
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "blackList"
})
@XmlRootElement(name = "getBlackListResponse")
public class GetBlackListResponse {
    private List<BlackList> blackList;

    public List<BlackList> getBlackList() {
        return blackList;
    }

    public void setBlackList(List<BlackList> blackList) {
        this.blackList = blackList;
    }
}
