package com.sipapp.soap.classes;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "Abonent"
})
@XmlRootElement(name = "deleteFromBlackListRequest")
public class DeleteFromBlackListRequest {
    @XmlElement(name = "id", required = true)
    private Long Abonent;

    public Long getIdAbonent() {
        return Abonent;
    }

    public void setIdAbonent(Long idAbonent) {
        this.Abonent = idAbonent;
    }
}
