package com.sipapp.controllers;

import com.sipapp.models.Abonent;
import com.sipapp.services.GuiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class SoapController {

    private final GuiService service;

    @Autowired
    public SoapController(GuiService service) {
        this.service = service;
    }

    @PayloadRoot(namespace = "http://localhost:8080", localPart = "addAbonent")
    @ResponsePayload
    public void addAbonent(@RequestPayload Abonent abonent){
        service.addAbonent(abonent);
    }
}
