package com.sipapp.controllers;

import com.sipapp.models.Abonent;
import com.sipapp.services.GuiService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;

@Controller
public class GuiController {


    private final GuiService service;

    public GuiController(GuiService service) {
        this.service = service;
    }

    @GetMapping("/index")
    public String index(){
        return "Index";
    }

    @GetMapping("/auto")
    public String authorization(){
        return "Authorization";
    }

    @GetMapping("/delete/{id}")
    public String deletePage(@PathVariable String id, Model model) {
        Abonent abonent = service.findAbonentById(Long.parseLong(id));
        model.addAttribute("abonent",abonent);
        return "Delete";
    }

    @GetMapping("/edit/{id}")
    public String editPage(@PathVariable String id, Model model) {
        System.out.println(id);
        Abonent abonent = service.findAbonentById(Long.parseLong(id));
        model.addAttribute("abonent",abonent);
        return "Edit";
    }

    @GetMapping("/reg")
    public String registrationPage() {
        return "Registration";
    }

    @GetMapping("/out")
    public String outPage(Model model){
        model.addAttribute("list",service.getAbonents());
        return "Output";
    }

    @RequestMapping(value = "/reg", method = RequestMethod.POST)
    public String saveAbonent(@ModelAttribute("abonent")Abonent abonent){
        return service.addAbonent(abonent);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
    public String deleteAbonent(@PathVariable String id){
        return service.deleteAbonent(Long.parseLong(id));
    }

    @RequestMapping(value = "/edit/update", method = RequestMethod.POST)
    public String editAbonent(@ModelAttribute Abonent abonent){
        return service.editAbonent(abonent);
    }
    @RequestMapping(value = "/out", method = RequestMethod.POST)
    public ModelAndView autoAbonent(@ModelAttribute Abonent abonent){
        if(service.authorization(abonent)){
            return new ModelAndView("redirect:/out");
        }
        else {
            return new ModelAndView("Failure");
        }
    }
}
