create database if not exists statistika;
use statistika;

create table if not exists Statistics
 
(
	  app_name  varchar(50) not null ,
    counter_name  varchar(50) not null ,
	  counter_value  long null,
    constraint uniqe_name unique(app_name, counter_name)
)
;
