package com.sipapp.service;

import com.sipapp.models.Counter;

import java.io.IOException;

public interface CounterService {

    Counter createCounter(Counter counter);

    void saveStatistic() throws IOException;
}
