package com.sipapp.service;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.sipapp.models.Counter;
import com.sipapp.models.Descriptor;
import com.sipapp.models.Counters;
import com.sipapp.repository.CounterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.*;

@Service
public class CounterServiceImpl implements CounterService {

    @Value("${counters.descriptors.path}")
    private String COUNTERS_DESCRIPTION_PATH;

    @Value("${counters.path}")
    private String COUNTERS_PATH;

    private XmlMapper mapper;
    private File xmlFile;
    private Descriptor descriptor;

    private final CounterRepository repository;
    private final Counters counters;

    @Autowired
    public CounterServiceImpl(CounterRepository repository, Counters counters) {
        this.repository = repository;
        this.counters = counters;
        mapper = new XmlMapper();
    }

    @Override
    public Counter createCounter(Counter counter) {
        boolean flag = false;
        for (String item : descriptor.getCounters()) {
            if (item.equals(counter.getCounterName()))
                flag = true;
        }
        if (flag) {
            for (Counter item : counters.getCounters()) {
                if (item.getCounterName().toLowerCase().equals(counter.getCounterName().toLowerCase())) {
                    item.setCounterValue(item.getCounterValue() + 1);
                }
            }
        }
        return counter;
    }

    @Override
    public void saveStatistic() throws IOException {
        boolean flag = false;
        xmlFile = new File(COUNTERS_PATH);
        Counters bufCounters = mapper.readValue(xmlFile, Counters.class);
        if (counters.getCounters().size() > bufCounters.getCounters().size()) {
            repository.saveAll(counters.getCounters());
        } else {
            for (int i = 0; i < counters.getCounters().size(); i++) {
                if (counters.getCounters().get(i).getCounterName().toLowerCase().equals(bufCounters.getCounters().get(i).getCounterName().toLowerCase()) &&
                        counters.getCounters().get(i).getCounterValue() != bufCounters.getCounters().get(i).getCounterValue()) {
                    flag = true;
                    break;
                }
            }
        }
        if (flag) {
            repository.saveAll(counters.getCounters());
        }
        mapper.writeValue(xmlFile, counters);
    }

    @PostConstruct
    private void init() throws IOException {
        COUNTERS_PATH = System.getProperty("user.dir")  + "\\" + COUNTERS_PATH;
        xmlFile = new File(COUNTERS_PATH);
        InputStream is = CounterServiceImpl.class.getResourceAsStream(COUNTERS_DESCRIPTION_PATH);
        descriptor = mapper.readValue(is, Descriptor.class);
        if (xmlFile.exists()) {
            Counters bufCounters = mapper.readValue(xmlFile, Counters.class);
            counters.setCounters(bufCounters.getCounters());
        } else {
            for (int i = 0; i < descriptor.getCounters().size(); i++)
                counters.addCounter(new Counter(descriptor.getCounters().get(i)));
            xmlFile = new File(COUNTERS_PATH);
            mapper.writeValue(xmlFile, counters);
        }
        repository.saveAll(counters.getCounters());
    }

}
