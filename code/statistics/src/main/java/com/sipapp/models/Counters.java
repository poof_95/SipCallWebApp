package com.sipapp.models;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "statistics")
@XmlAccessorType(XmlAccessType.NONE)
public class Counters {

    @XmlElement(name = "counters")
    private List<Counter> counters = new ArrayList<>();

    public Counters() {
    }

    public void setCounters(List<Counter> counters) {
        this.counters = counters;
    }

    public List<Counter> getCounters() {
        return counters;
    }

    public void addCounter(Counter counter) {
        counters.add(counter);
    }

}
