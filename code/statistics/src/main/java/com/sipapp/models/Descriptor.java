package com.sipapp.models;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "statistics_description")
public class Descriptor {
    @XmlElement
    private String appName;
    @XmlElement(name = "counter")
    private List<String> counters;

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public List<String> getCounters() {
        return counters;
    }

    public void setCounters(List<String> counters) {
        this.counters = counters;
    }
}
