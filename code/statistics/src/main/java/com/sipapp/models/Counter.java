package com.sipapp.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "counters")
@XmlRootElement(name = "counter")
public class Counter {

    @Column(name = "app_name")
    @XmlElement
    private String appName;
    @Id
    @Column(name = "counter_name")
    @XmlElement
    private String counterName;
    @XmlElement
    @Column(name = "counter_value")
    private Long counterValue;

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getCounterName() {
        return counterName;
    }

    public void setCounterName(String counterName) {
        this.counterName = counterName;
    }

    public Long getCounterValue() {
        return counterValue;
    }

    public void setCounterValue(Long counterValue) {
        this.counterValue = counterValue;
    }

    public Counter() {
    }

    public Counter(String appName, String counterName, Long counterValue) {
        this.appName = appName;
        this.counterName = counterName;
        this.counterValue = counterValue;
    }

    public Counter(String counterName) {
        this.counterName = counterName;
        this.appName = "SipApp";
        this.counterValue = 0L;
    }

    @Override
    public String toString() {
        return "Counter{" +
                "appName='" + appName + '\'' +
                ", counterName='" + counterName + '\'' +
                ", counterValue='" + counterValue + '\'' +
                '}';
    }
}
