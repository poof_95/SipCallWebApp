package com.sipapp.controller;


import com.sipapp.models.Counter;
import com.sipapp.service.CounterService;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;


@RestController
public class CounterController {

    private final CounterService service;

    public CounterController(CounterService service) {
        this.service = service;
    }

    @Scheduled(fixedRateString = "${prop.timeout}000")
    @Async
    public void saveCounters() throws IOException {
        service.saveStatistic();
    }

    @RequestMapping(value = "/set", method = RequestMethod.POST)
    public Counter saveCounter(@RequestBody Counter counter) {
        return service.createCounter(counter);
    }

}
