package com.sipapp.repository;

import com.sipapp.modells.DescriptionAlarm;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DescriptionRepository extends CrudRepository<DescriptionAlarm,Long> {
}
