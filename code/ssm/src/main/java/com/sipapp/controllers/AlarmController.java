package com.sipapp.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sipapp.modells.Alarm;
import com.sipapp.modells.DescriptionAlarm;
import com.sipapp.modells.ErrorAlarm;
import com.sipapp.services.AlarmService;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;
import sun.net.www.protocol.http.HttpURLConnection;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;

@RestController
public class AlarmController  {

    private final AlarmService service;

    @Value("${telegram.port}")
    private String TELEGRAM_PORT;

    private URL url;
    private URLConnection con;
    private HttpURLConnection http;
    private OutputStream os;
    private ObjectMapper jsonMapper;

    public AlarmController(AlarmService service) {
        this.service = service;
    }

    @Scheduled(fixedRateString = "${prop.timeout}000")
    @Async
    public void saveAlarms() throws IOException {
        service.saveAlarms();
    }

    @RequestMapping(value = "/set", method = RequestMethod.POST)
    public Alarm addAlarm(@RequestBody ErrorAlarm errorAlarm) throws IOException{
        sendAlarm(errorAlarm);
        return service.createAlarm(errorAlarm);
    }

    @Async
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public void updateAlarm(@RequestBody ErrorAlarm errorAlarm) throws IOException{
        service.updateAlarm(errorAlarm);
    }

    @Async
    public void sendAlarm(ErrorAlarm errorAlarm) throws IOException{
        con = url.openConnection();
        http = (HttpURLConnection)con;
        http.setRequestMethod("POST");
        http.setDoOutput(true);
        String jsonStr = jsonMapper.writeValueAsString(service.sendDescriptionAlarm(errorAlarm));
        byte[] out = jsonStr.getBytes(StandardCharsets.UTF_8);
        int length = out.length;
        http.setFixedLengthStreamingMode(length);
        http.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        http.connect();
        os = http.getOutputStream();
        os.write(out);
        os.close();
    }

    @PostConstruct
    private void init() throws IOException{
        url = new URL(TELEGRAM_PORT);
        jsonMapper = new ObjectMapper();
    }

}
