package com.sipapp.modells;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@JacksonXmlRootElement(localName = "alarmDefinitions")
@XmlAccessorType(XmlAccessType.NONE)
public class Alarms {


    @XmlElement
    private List<Alarm> alarms = new ArrayList<>();

    public Alarms() {
    }

    public void addAlarm(Alarm alarm){
        alarms.add(alarm);
    }

    public List<Alarm> getAlarms(){
        return alarms;
    }

    public void setAlarms(List<Alarm> alarms){
        this.alarms = alarms;
    }

}
