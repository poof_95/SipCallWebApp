package com.sipapp.modells;

import java.util.Date;

public class ErrorAlarm {

    private long idAlarms;

    private Date dateRaised;

    public ErrorAlarm() {
    }

    public ErrorAlarm(long idAlarms, Date dateRaised) {
        this.idAlarms = idAlarms;
        this.dateRaised = dateRaised;
    }

    public long getIdAlarms() {
        return idAlarms;
    }

    public void setIdAlarms(long idAlarms) {
        this.idAlarms = idAlarms;
    }

    public Date getDateRaised() {
        return dateRaised;
    }

    public void setDateRaised(Date dateRaised) {
        this.dateRaised = dateRaised;
    }

    @Override
    public String toString() {
        return "ErrorAlarm{" +
                "idAlarms=" + idAlarms +
                ", dateRaised=" + dateRaised +
                '}';
    }
}
