package com.sipapp.modells;

import javax.persistence.*;

@Entity(name = "description_alarms")
public class DescriptionAlarm {
    @Id
    @Column
    private long errorCode;
    @Column(length = 20)
    private String appName;
    @Column
    private int eventType;
    @Column
    private String alarmDescription;
    @Column
    private String activeDescription;

    public DescriptionAlarm() {
    }

    public DescriptionAlarm(long errorCode, String appName, int eventType, String alarmDescription, String activeDescription) {
        this.errorCode = errorCode;
        this.appName = appName;
        this.eventType = eventType;
        this.alarmDescription = alarmDescription;
        this.activeDescription = activeDescription;
    }

    public long getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(long errorCode) {
        this.errorCode = errorCode;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public int getEventType() {
        return eventType;
    }

    public void setEventType(int eventType) {
        this.eventType = eventType;
    }

    public String getAlarmDescription() {
        return alarmDescription;
    }

    public void setAlarmDescription(String alarmDescription) {
        this.alarmDescription = alarmDescription;
    }

    public String getActiveDescription() {
        return activeDescription;
    }

    public void setActiveDescription(String activeDescription) {
        this.activeDescription = activeDescription;
    }

    public void copy(DescriptionAlarm copy){
        this.errorCode = copy.errorCode;
        this.appName = copy.appName;
        this.eventType = copy.eventType;
        this.alarmDescription = copy.alarmDescription;
        this.activeDescription = copy.activeDescription;
    }

    @Override
    public String toString() {
        return "DescriptionAlarm{" +
                "errorCode=" + errorCode +
                ", appName='" + appName + '\'' +
                ", eventType=" + eventType +
                ", alarmDescription='" + alarmDescription + '\'' +
                ", activeDescription='" + activeDescription + '\'' +
                '}';
    }
}
