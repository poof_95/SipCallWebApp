package com.sipapp.modells;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.Date;

@JacksonXmlRootElement(localName = "alarm")
@Entity(name = "alarms")
@XmlAccessorType(value = XmlAccessType.FIELD)
public class Alarm {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)
    @XmlElement
    private int idAlarms;
    @Column
    @XmlElement
    private long errorCode;
    @Column
    @XmlElement
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date dateRaised;
    @Column
    @XmlElement
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date dateClear;
    @Column
    @XmlElement
    private boolean status;

    public Alarm() {
    }

    public Alarm(long errorCode) {
        this.errorCode = errorCode;
        this.dateRaised = new Date();
        this.dateClear = new Date();
        this.status = true;
    }

    public Alarm(long errorCode, Date dateRaised, Date dateClear, boolean status) {
        this.errorCode = errorCode;
        this.dateRaised = dateRaised;
        this.dateClear = dateClear;
        this.status = status;
    }

    public int getIdAlarms() {
        return idAlarms;
    }

    public void setIdAlarms(int idAlarms) {
        this.idAlarms = idAlarms;
    }

    public long getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(long errorCode) {
        this.errorCode = errorCode;
    }

    public Date getDateRaised() {
        return dateRaised;
    }

    public void setDateRaised(Date dateRaised) {
        this.dateRaised = dateRaised;
    }

    public Date getDateClear() {
        return dateClear;
    }

    public void setDateClear(Date dateClear) {
        this.dateClear = dateClear;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public void copy(Alarm copy){
        this.errorCode = copy.errorCode;
        this.dateRaised = copy.dateRaised;
        this.dateClear = copy.dateClear;
        this.status = copy.status;
    }

    @Override
    public String toString() {
        return "Alarm{" +
                "idAlarms=" + idAlarms +
                ", errorCode=" + errorCode +
                ", dateRaised=" + dateRaised +
                ", dateClear=" + dateClear +
                ", status=" + status +
                '}';
    }
}
