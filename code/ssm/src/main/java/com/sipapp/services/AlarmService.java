package com.sipapp.services;

import com.sipapp.modells.Alarm;
import com.sipapp.modells.DescriptionAlarm;
import com.sipapp.modells.ErrorAlarm;

import java.io.IOException;
import java.util.List;

public interface AlarmService {

    Alarm createAlarm(ErrorAlarm errorAlarm) throws IOException;

    List<Alarm> getAlarms();

    void saveAlarms() throws IOException;

    void updateAlarm(ErrorAlarm errorAlarm)  throws IOException;

    DescriptionAlarm sendDescriptionAlarm(ErrorAlarm errorAlarm);

}
