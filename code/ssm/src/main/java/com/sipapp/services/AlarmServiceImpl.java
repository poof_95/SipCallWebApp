package com.sipapp.services;


import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.sipapp.modells.Alarm;
import com.sipapp.modells.Alarms;
import com.sipapp.modells.DescriptionAlarm;
import com.sipapp.modells.ErrorAlarm;
import com.sipapp.repository.AlarmRepository;
import com.sipapp.repository.DescriptionRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AlarmServiceImpl implements AlarmService {

    private final AlarmRepository alarmRepository;
    private final DescriptionRepository descriptionRepository;
    private final Alarms alarms;

    private XmlMapper mapper;
    private File xmlFile;

    private DescriptionAlarm descriptionAlarm;

    @Value("${alarm.description.path}")
    private String ALARM_DESCRIPTION_PATH;

    public AlarmServiceImpl(AlarmRepository alarmRepository, DescriptionRepository descriptionRepository, Alarms alarms) {
        this.alarmRepository = alarmRepository;
        this.descriptionRepository = descriptionRepository;
        this.alarms = alarms;
        mapper = new XmlMapper();
    }

    @Override
    public Alarm createAlarm(ErrorAlarm errorAlarm) throws IOException {
        Optional<DescriptionAlarm> descriptionAlarms = descriptionRepository.findById(errorAlarm.getIdAlarms());
        Alarm alarm = new Alarm();
        alarm.setDateRaised(errorAlarm.getDateRaised());
        alarm.setErrorCode(errorAlarm.getIdAlarms());
        descriptionAlarm = descriptionAlarms.get();
        alarm.setStatus((descriptionAlarm.getEventType() == 2) ? true : false);
        if (alarm.isStatus())
            alarm.setDateClear(alarm.getDateRaised());
        alarms.addAlarm(alarm);
        return alarm;
    }

    @Override
    public List<Alarm> getAlarms() {
        return (List<Alarm>) alarmRepository.findAll();
    }

    @Override
    public void saveAlarms() throws IOException {
        Alarms bufAlarm = mapper.readValue(xmlFile, Alarms.class);
        if (alarms.getAlarms().size() > bufAlarm.getAlarms().size()) {
            alarmRepository.saveAll(alarms.getAlarms());
        }else if (alarms.getAlarms().size() == bufAlarm.getAlarms().size()){
            for (int i = 0; i < alarms.getAlarms().size(); i++) {
                if (alarms.getAlarms().get(i).isStatus() != bufAlarm.getAlarms().get(i).isStatus()){
                    alarmRepository.saveAll(alarms.getAlarms());
                    break;
                }
            }
        }
        mapper.writeValue(xmlFile, alarms);
    }

    @Override
    public void updateAlarm(ErrorAlarm errorAlarm)  throws IOException{
        for (Alarm alarm : alarms.getAlarms()) {
            if (!alarm.isStatus() && alarm.getErrorCode() == errorAlarm.getIdAlarms()){
                alarm.setStatus(true);
                alarm.setDateClear(errorAlarm.getDateRaised());
                break;
            }
        }
    }

    @Override
    public DescriptionAlarm sendDescriptionAlarm(ErrorAlarm errorAlarm) {
        Optional<DescriptionAlarm> descriptionAlarms = descriptionRepository.findById(errorAlarm.getIdAlarms());
        return descriptionAlarms.get();
    }

    @PostConstruct
    private void init() throws IOException {
        xmlFile = new File(ALARM_DESCRIPTION_PATH);
        if (xmlFile.exists()) {
            Alarms bufAlarms = mapper.readValue(xmlFile, Alarms.class);
            alarms.setAlarms(bufAlarms.getAlarms());
            alarmRepository.saveAll(alarms.getAlarms());
        } else {
            Alarm alarm = new Alarm(-1l);
            alarms.addAlarm(alarm);
            mapper.writeValue(xmlFile, alarms);
            alarmRepository.saveAll(alarms.getAlarms());
        }
//                List<DescriptionAlarm> descriptionAlarms = new ArrayList<>();
//        String[] arr = {
//                "000000;MySql;1;MySQL Health check: MySQL daemon was shutdown, critical;Run MySql daemon!!!",
//                "111111;Telegram;1;Telegram connect: Connect telegram bot lost, critical;Check connection telegram bot, run bot!!",
//                "222222;MySql;2;MySQL runtime error: MySQL system time error, not critical;Check system time",
//                "333333;ApacheTomcat;1;Tomcat Health check: Tomcat start port error, critical;Check start port ",
//                "444444;Telegram;2;Telegram name error: telegram bot name change,not critical;Refresh telegram bot name",
//                "555555;SipApp;1;SipApp Health check: SipApp lost connection to Statistics module, critical;Check and run Statistics module",
//                "666666;StatisticModule;1;Statistics Health check: Statistics lost connection to database, critical;Check connection to database",
//                "777777;SipApp;2;SipApp Time check:SipApp system time change, not critical;Check system time",
//                "888888;GUI;2;GUI Font check: GUI error loading font for page, not critical;Check font file",
//                "999999;GUI;1;GUI Health check: GUI lost connection to database, critical;Check database"
//        };
//        for (int i = 0; i < arr.length; i++) {
//            String[] arrBuf = arr[i].split(";");
//            DescriptionAlarm descriptionAlarm = new DescriptionAlarm();
//            descriptionAlarm.setErrorCode(Long.parseLong(arrBuf[0]));
//            descriptionAlarm.setAppName(arrBuf[1]);
//            descriptionAlarm.setEventType(Integer.parseInt(arrBuf[2]));
//            descriptionAlarm.setAlarmDescription(arrBuf[3]);
//            descriptionAlarm.setActiveDescription(arrBuf[4]);
//            descriptionAlarms.add(descriptionAlarm);
//        }
//        descriptionRepository.saveAll(descriptionAlarms);
    }

}
