create database if not exists database_alarm ;
use database_alarm;

create table if not exists alarms
(
	idAlarms  integer primary key auto_increment,
    errorCode long,
	dateRaised  datetime,
	dateClear  datetime,
	status  bool
)
;

create table if not exists description_alarms
(
	   errorCode long primary key,
    appName varchar(20),
    eventType integer,
    alarmDescription text,
    activeDescription text
  
)
;

