insert into dbssm.description_alarms(error_code,app_name,event_type, alarm_description,active_description) values(000000,'MySql',1,'MySQL Health check: MySQL daemon was shutdown, critical','Run MySql daemon!!!');

insert into dbssm.description_alarms(error_code,app_name,event_type, alarm_description,active_description) values(111111,'Telegram',2,'MySQL runtime error: MySQL system time error, not critical','Check connection telegram bot, run bot!!');

insert into dbssm.description_alarms(error_code,app_name,event_type, alarm_description,active_description) values(222222,'MySql',2,'MySQL Health check: MySQL daemon was shutdown, critical','Check system time');

insert into dbssm.description_alarms(error_code,app_name,event_type, alarm_description,active_description) values(333333,'ApacheTomcat',1,'Tomcat Health check: Tomcat start port error, critical','Check start port');

insert into dbssm.description_alarms(error_code,app_name,event_type, alarm_description,active_description) values(444444,'Telegram',2,'Telegram name error: telegram bot name change,not critical','Refresh telegram bot name');

insert into dbssm.description_alarms(error_code,app_name,event_type, alarm_description,active_description) values(555555,'SipApp',1,'MySQL Health check: MySQL daemon was shutdown, critical','Run MySql daemon!!!');

insert into dbssm.description_alarms(error_code,app_name,event_type, alarm_description,active_description) values(666666,'StatisticModule',1,'Statistics Health check: Statistics lost connection to database critical','Check connection to database');

insert into dbssm.description_alarms(error_code,app_name,event_type, alarm_description,active_description) values(777777,'SipApp',2,'SipApp Time check:SipApp system time change, not critical','Check system time');

insert into dbssm.description_alarms(error_code,app_name,event_type, alarm_description,active_description) values(888888,'GUI',2,'GUI Font check: GUI error loading font for page, not critical','Check font file');

insert into dbssm.description_alarms(error_code,app_name,event_type, alarm_description,active_description) values(999999,'GUI',2,'GUI Health check: GUI lost connection to database, critical','Check database');
