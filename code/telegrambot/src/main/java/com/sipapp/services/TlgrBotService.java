package com.sipapp.services;

import com.sipapp.models.Alarm;

public interface TlgrBotService {
    Alarm sendMessage(Alarm alarm);
}
