package com.sipapp.services;

import com.sipapp.core.BotCore;
import com.sipapp.models.Alarm;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.exceptions.TelegramApiRequestException;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Service
public class TlgrBotServiceImpl implements TlgrBotService {

    private final TelegramBotsApi telegramBotsApi;

    private BotCore alarmBot;

    public TlgrBotServiceImpl(TelegramBotsApi telegramBotsApi) {
        this.telegramBotsApi = telegramBotsApi;
    }

    @Override
    public Alarm sendMessage(Alarm alarm) {
        alarmBot.sendAlam(alarm.toString());
        return alarm;
    }

    @PostConstruct
    private void init() throws IOException {
        alarmBot = new BotCore();
        try {
            telegramBotsApi.registerBot(alarmBot);
        } catch (TelegramApiRequestException e) {
            e.printStackTrace();
        }
    }
}
