package com.sipapp.models;

public class Alarm {

    private long errorCode;
    private String appName;
    private int eventType;
    private String alarmDescription;
    private String activeDescription;

    public Alarm() {
    }

    public Alarm(long errorCode, String appName, int eventType, String alarmDescription, String activeDescription) {
        this.errorCode = errorCode;
        this.appName = appName;
        this.eventType = eventType;
        this.alarmDescription = alarmDescription;
        this.activeDescription = activeDescription;
    }

    public long getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(long errorCode) {
        this.errorCode = errorCode;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public int getEventType() {
        return eventType;
    }

    public void setEventType(int eventType) {
        this.eventType = eventType;
    }

    public String getAlarmDescription() {
        return alarmDescription;
    }

    public void setAlarmDescription(String alarmDescription) {
        this.alarmDescription = alarmDescription;
    }

    public String getActiveDescription() {
        return activeDescription;
    }

    public void setActiveDescription(String activeDescription) {
        this.activeDescription = activeDescription;
    }

    @Override
    public String toString() {
        return "Alarm:" +
                "\nerrorCode: " + errorCode +
                "\nappName: '" + appName + '\'' +
                "\neventType: " + eventType +
                "\nalarmDescription: '" + alarmDescription + '\'' +
                "\nactiveDescription: '" + activeDescription + '\'';
    }
}
