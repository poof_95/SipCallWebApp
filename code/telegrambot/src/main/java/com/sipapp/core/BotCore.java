package com.sipapp.core;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.util.HashSet;
import java.util.Set;

public class BotCore extends TelegramLongPollingBot {

    private static Set<Long> chats = new HashSet<>();

    private static final String DEFAULT_MESSAGE = "Unknown command!!!\nBefore started write command \"/startAlarmBot\"";
    private static final String HELLO_MESSAGE = "Alarm bot started!!!";
    private static final String BOT_TOKEN = "608071161:AAEACO8Kau_u2ZJ7GTcYf3WL41WdzWr9704";
    private static final String BOT_NAME = "SipAlarmBot";

    public void sendAlam(String text) {
        for (Long id : chats) {
            SendMessage message = new SendMessage();
            message.setText(text);
            message.setChatId(id);
            sendMsg(message);
        }
    }

    private void sendMsg(SendMessage message) {
        try {
            sendMessage(message);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()) {
            switch (update.getMessage().getText()) {
                case "/startAlarmBot": {
                    chats.add(update.getMessage().getChatId());
                    SendMessage message = new SendMessage();
                    message.setChatId(update.getMessage().getChatId());
                    message.setText(HELLO_MESSAGE);
                    sendMsg(message);
                    chats.add(update.getMessage().getChatId());
                }
                break;
                default: {
                    SendMessage message = new SendMessage();
                    for (Long chatId : chats) {
                        if (chatId == update.getMessage().getChatId()) {
                            chats.remove(chatId);
                            break;
                        }
                    }
                    message.setChatId(update.getMessage().getChatId());
                    message.setText(DEFAULT_MESSAGE);
                    sendMsg(message);
                }

            }
        }
    }

    @Override
    public String getBotToken() {
        return BOT_TOKEN;
    }

    @Override
    public String getBotUsername() {
        return BOT_NAME;
    }
}
