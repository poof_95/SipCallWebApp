package com.sipapp.controllers;

import com.sipapp.models.Alarm;
import com.sipapp.services.TlgrBotService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TlgrBotController {

    private final TlgrBotService service;

    public TlgrBotController(TlgrBotService service) {
        this.service = service;
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public Alarm saveCounter(@RequestBody Alarm alarm) {
        return service.sendMessage(alarm);
    }

}
