package com.sipapp.conf;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.telegram.telegrambots.TelegramBotsApi;

@Configuration
public class TlgrBotConf {

    @Bean
    public PropertySourcesPlaceholderConfigurer placeholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    public TelegramBotsApi getTeleframBotsApi(){
        return new TelegramBotsApi();
    }
}
