package com.sipapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.telegram.telegrambots.ApiContextInitializer;

@SpringBootApplication
@PropertySource("classpath:config.properties")
public class SpringTlgrBot {
    public static void main(String[] args) {
        ApiContextInitializer.init();
        SpringApplication.run(SpringTlgrBot.class, args);
    }
}
