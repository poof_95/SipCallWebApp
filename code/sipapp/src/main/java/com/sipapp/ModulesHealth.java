package com.sipapp;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import static com.sipapp.HttpSip.*;
import static com.sipapp.ProxyRepo.getErrorCode;

public final class ModulesHealth extends Thread {

    private final String MODULE_DESCRIPTION_SUCCESS = " module is work!";
    private final String MODULE_DESCRIPTION_FAIL = " module not work!";

    private DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ssz");

    public ModulesHealth(){
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    private String[] hostList = {
            "http://localhost:8081/",
            "http://localhost:8082/",
            "http://localhost:8083/",
            "http://localhost:8084/"};

    private String[] hostListName = {
            "GUI",
            "Statistics",
            "SSM",
            "Telegram"};


    @Override
    public void run() {
        while (true) {
            try {
                sleep(60000);
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
            System.out.println("===========" + dateFormat.format(new Date()) + "===========");
            for (int i = 0; i < hostList.length; i++) {
                if (getStatusModule(hostList[i]).equals(ERROR_STATUS)) {
                    System.out.println(hostListName[i] + MODULE_DESCRIPTION_FAIL);
                    getErrorCode(hostListName[i], true);
                } else {
                    System.out.println(hostListName[i] + MODULE_DESCRIPTION_SUCCESS);
                    getErrorCode(hostListName[i], false);
                }
            }
        }
    }
}
