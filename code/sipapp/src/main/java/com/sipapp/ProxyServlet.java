package com.sipapp;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.sip.*;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.sipapp.ProxyRepo.check;

public class ProxyServlet extends javax.servlet.sip.SipServlet implements SipErrorListener, Servlet {

    private static ModulesHealth modulesHealth = new ModulesHealth();
    public ProxyServlet() {
        modulesHealth.start();
    }

    @Override
    protected void doInvite(SipServletRequest request) throws TooManyHopsException, IOException {
        if(request.isInitial()){
            Proxy proxy = request.getProxy();
            proxy.setRecordRoute(true);
            proxy.setSupervised(true);
            Pattern pattern = Pattern.compile("\\+\\d+");
            Matcher matcher = pattern.matcher(request.getRequestURI().toString());
            if(matcher.find()){
                String number = matcher.group();
                System.out.println("Incoming call,number: " + number);
                if(check(number)){
                    System.out.println("Call Success, number: " + number);
                    proxy.proxyTo(request.getRequestURI());
                }
                else{
                    System.out.println("Call Blocked or Failure, number: " + number);
                    SipServletResponse response = request.createResponse(SipServletResponse.SC_FORBIDDEN);
                    response.send();
                }
            }
        }
    }

    @Override
    protected void doBye(SipServletRequest request) throws ServletException, IOException {
       // System.out.println("ProxyServlet: Got Bye request:\n" + request);
        super.doBye(request);
    }

    @Override
    protected void doResponse(SipServletResponse response) throws ServletException, IOException {
        //System.out.println("ProxyServlet: Got responce:\n" + response);
        super.doResponse(response);
    }


    public void noAckReceived(SipErrorEvent sipErrorEvent) {
        //System.out.println("ProxyServlet: Error:noAckReceived");
    }

    public void noPrackReceived(SipErrorEvent sipErrorEvent) {
        //System.out.println("ProxyServlet: Error: noPrackReceived");
    }

}
