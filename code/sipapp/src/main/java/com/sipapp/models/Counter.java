package com.sipapp.models;

public class Counter {

    private String appName;
    private String counterName;
    private Long counterValue;

    public Counter() {
    }

    public Counter(String appName, String counterName, Long counterValue) {
        this.appName = appName;
        this.counterName = counterName;
        this.counterValue = counterValue;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getCounterName() {
        return counterName;
    }

    public void setCounterName(String counterName) {
        this.counterName = counterName;
    }

    public Long getCounterValue() {
        return counterValue;
    }

    public void setCounterValue(Long counterValue) {
        this.counterValue = counterValue;
    }

    @Override
    public String toString() {
        return "Counter{" +
                "appName='" + appName + '\'' +
                ", counterName='" + counterName + '\'' +
                ", counterValue=" + counterValue +
                '}';
    }
}
