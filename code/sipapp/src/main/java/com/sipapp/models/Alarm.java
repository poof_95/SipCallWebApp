package com.sipapp.models;

import java.util.Date;

public class Alarm {

    private Long idAlarms;
    private Date dateRaised;

    public Alarm() {
    }

    public Alarm(Long idAlarms, Date dateRaised) {
        this.idAlarms = idAlarms;
        this.dateRaised = dateRaised;
    }

    public Long getIdAlarms() {
        return idAlarms;
    }

    public void setIdAlarms(Long idAlarms) {
        this.idAlarms = idAlarms;
    }

    public Date getDateRaised() {
        return dateRaised;
    }

    public void setDateRaised(Date dateRaised) {
        this.dateRaised = dateRaised;
    }

    @Override
    public String toString() {
        return "Alarm{" +
                "idAlarms=" + idAlarms +
                ", dateRaised=" + dateRaised +
                '}';
    }
}
