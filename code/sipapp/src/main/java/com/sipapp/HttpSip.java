package com.sipapp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sipapp.models.Alarm;
import com.sipapp.models.Counter;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Date;

public abstract class HttpSip {

    public static final String MSG_SUCCESS = "Success";
    public static final String MSG_BLOCKED = "Blocked";
    public static final String MSG_FAILURE = "Failure";

    public static final String ERROR_STATUS = "ERROR";
    public static final String OK_STATUS = "OK";

    private static final String URL_STATISTIC = "http://localhost:8082/set";
    private static final String URL_SSM_SET = "http://localhost:8083/set";
    private static final String URL_SSM_UPDATE = "http://localhost:8083/update";

    public static void sendStatisticMessage(String counterName) {
        try {
            URL url = new URL(URL_STATISTIC);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            Counter counter = new Counter("SipApp",counterName,1L);
            ObjectMapper jsonMapper = new ObjectMapper();
            String json = jsonMapper.writeValueAsString(counter);
            connection.setFixedLengthStreamingMode(json.getBytes("UTF-8").length);
            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            DataOutputStream dos = new DataOutputStream(connection.getOutputStream());
            dos.write(json.getBytes("UTF-8"));
            dos.flush();
            dos.close();
            connection.disconnect();
        } catch (MalformedURLException e) {
            System.out.println(e.getMessage());
        } catch (ProtocolException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void sendSsmMessage(String idAlarm, boolean type) {
        try {
            URL url = new URL((type) ? URL_SSM_SET : URL_SSM_UPDATE);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            ObjectMapper jsonMapper = new ObjectMapper();
            Alarm alarm = new Alarm(Long.parseLong(idAlarm),new Date());
            String json = jsonMapper.writeValueAsString(alarm);
            connection.setFixedLengthStreamingMode(json.getBytes("UTF-8").length);
            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            DataOutputStream dos = new DataOutputStream(connection.getOutputStream());
            dos.write(json.getBytes("UTF-8"));
            dos.flush();
            dos.close();
            connection.disconnect();
        } catch (MalformedURLException e) {
            System.out.println(e.getMessage());
        } catch (ProtocolException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public static String getStatusModule(String url){
        String result = null;
        try{
            URL moduleURL = new URL(url);
            HttpURLConnection httpURLConnection = (HttpURLConnection) moduleURL.openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.connect();
            result = OK_STATUS;
            httpURLConnection.disconnect();
        }catch (Exception e){
            result = ERROR_STATUS;
        }
        return result;
    }

}
