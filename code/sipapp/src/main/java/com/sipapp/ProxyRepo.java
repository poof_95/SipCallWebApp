package com.sipapp;

import java.sql.*;

import static com.sipapp.HttpSip.*;

public abstract class ProxyRepo {

    private static final String URL_GUI = "jdbc:mysql://localhost:3306/dbgui";
    private static final String URL_SSM = "jdbc:mysql://localhost:3306/dbssm";
    private static final String USER = "root";
    private static final String PASSWORD = "1234";


    private static final String QUERY_WHITE_LIST = "select phone_number from dbgui.phone_numbers where id_phone_number = " +
                                                   "(select id_phone_number from abonent where id_phone_number = " +
                                                   "(select id_phone_number from white_list)) and phone_number = ";

    private static final String QUERY_BLACK_LIST = "select phone_number from dbgui.phone_numbers where id_phone_number = " +
                                                   "(select id_phone_number from dbgui.abonent where id_abonent = " +
                                                   "(select id_abonent from dbgui.black_list)) and phone_number = ";

    private static final String QUERY_PHONE_NUMBER = "select id_phone_number from phone_numbers where phone_number = ";

    private static final String QUERY_ERROR_CODE = "select error_code from dbssm.description_alarms where event_type = 1" +
                                                   " and app_name = ";


    public static boolean check(String phoneNumber){
        Connection con = null;
        Statement stm = null;
        ResultSet rs = null;
        try{
            con = DriverManager.getConnection(URL_GUI,USER,PASSWORD);
            stm = con.createStatement();
            rs = stm.executeQuery(QUERY_WHITE_LIST + phoneNumber);
            if(rs.next()){
                sendStatisticMessage(MSG_SUCCESS);
                return true;
            }
            else{
                rs = stm.executeQuery(QUERY_BLACK_LIST + phoneNumber);
                if(rs.next()) {
                    sendStatisticMessage(MSG_BLOCKED);
                    return false;
                }
                else {
                    rs = stm.executeQuery(QUERY_PHONE_NUMBER + phoneNumber);
                    if (rs.next()){
                        sendStatisticMessage(MSG_SUCCESS);
                        return true;
                    }else {
                        sendStatisticMessage(MSG_FAILURE);
                        return false;
                    }
                }
            }
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
        finally {
            try{
                con.close();
                stm.close();
                rs.close();
            }
            catch (SQLException e){
                System.out.println(e.getSQLState());
            }
        }
        sendStatisticMessage(MSG_FAILURE);
        return false;
    }

    public static void getErrorCode(String appName, boolean type){
        try{
            Connection con = DriverManager.getConnection(URL_SSM,USER,PASSWORD);
            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery(QUERY_ERROR_CODE + "'"+ appName +"'");
            if(rs.next()){
                sendSsmMessage(rs.getString("error_code"), type);
            }
            con.close();
            stm.close();
            rs.close();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
